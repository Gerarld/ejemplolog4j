package com.arquitectura.logs;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.Logger;
import java.lang.RuntimeException;


public class LogO2Niveles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PatternLayout patron = new PatternLayout("%m %n");
		ConsoleAppender consola = new ConsoleAppender(patron);
		Logger log = Logger.getLogger("milog");
		log.addAppender(consola);
		log.setLevel(Level.DEBUG);
		log.info(LogO2Niveles.calcularImporte(300.00));
		
//		log.fatal("realizando un log sencillo nivel FATAL");
//		log.error("realizando un log sencillo nivel Error");
//		log.warn("realizando un log sencillo nivel Warnning");
//		log.info("realizando un log sencillo nivel INFO");
//		log.debug("realizando un log sencillo nivel DEBUG");
//			
		
	}
	
	public static Logger crearLog() {
		PatternLayout patron = new PatternLayout("%m %n");
		ConsoleAppender consola = new ConsoleAppender(patron);
		Logger log = Logger.getLogger("milog");
		return log;
	}
	
	public static double calcularImporte(double importe)
	{
		Logger log = crearLog();
		log.info("entramos en la funcion calcuarImporte()");
		log.debug("el importe calculado es: " + importe);
		if(importe < 0){
			log.warn("el importe no puede ser negativo");
			throw new RuntimeException("operacion no soportada con el importe");
		} else {
				if(importe >0 && importe < 100){
					log.info("compra con el 10% de descuento");
					return importe * 0.90;
				}else{
					log.info("compra con el 20% de descuento");
					return importe * 0.80;
				}
		}		

	}

}
