package com.arquitectura.logs;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class Log02Usandolog {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		PatternLayout patron = new PatternLayout("%m %n");
		ConsoleAppender consola = new ConsoleAppender(patron);
		Logger log = Logger.getLogger("millog");
		log.addAppender(consola);
		log.info("Realizando un log sencillo ");

	}

}
